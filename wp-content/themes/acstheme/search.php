<?php get_header(); ?>
<div id="content" class="container search-page">
<?php if (have_posts()) : ?>

	<h4>Search Results</h4>

	<?php include (TEMPLATEPATH . '/inc/nav.php' ); ?>

	<?php while (have_posts()) : the_post(); ?>

	<article class="result-item" id="post-<?php the_ID(); ?>">
		<div class="row">
			<header>
				<h4><a href="<?php the_permalink(); ?>" title=""><?php the_title(); ?></a></h4>
				
			</header>

			<div class="entry">
				<?php the_excerpt(); ?>
			</div>
		</div>
	</article>

<?php endwhile; ?>

<?php include (TEMPLATEPATH . '/inc/nav.php' ); ?>

<?php else : ?>

	<h4>Sorry, no results found.</h4>

<?php endif; ?>
</div>

<?php get_footer(); ?>

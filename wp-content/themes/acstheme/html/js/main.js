$(document).ready(function() {

	doc = $(this);

	//initial values and define events
	init();

	//add event for button to show all languages.
	LanguageListButton_Click();

	//add event for button to show search textbox.
	SearchButton_Click();

	//add event for icon button to show nav-links in Student-Parent-Teacher (SPT) Group
	SPTIcon_Click();

	//add event for icon button to show News-event highlights panel
	NewsEventIcon_Click();

	//add event for browser resize window.
	Window_Resize();

});

var doc;	//document object

var ratio;		//ratio for height and width value of banner

var banner_min_height = 1100;

var banner_height;		//banner height;

var banner_width;

var half_banner_height;

function init()
{
	//init value

	ratio = 0.95;

	banner_height = 1100;

	banner_width = $('.banner').width();

	//Calculate height value for banner
	setBannerHeight();

	//Calculate width value for slot in studen-parent-teacher-group
	setSlotBannerWidth();
}

function LanguageListButton_Click() 
{

	var counter = 0;
	$('.search-language .language-switcher-ico').click(function(event) {
		/* Act on the odd event */
		if (++counter % 2 != 0) {
			var height = $('.language-list').height();
			$('.language-list').css('height', 0);
			$('.language-list').css('opacity', 0);
			$('.language-list').css('z-index', 9999);
			$('.language-list').show().animate({height: height, opacity: 1}, 400);
		/* Act on the even event */
		} else {	
			$('.language-list').animate(
				{
					height: '0', 
					opacity: 0,
				}, 400, function() {
					$(this).css({
						height: 'auto',
						'z-index': -1,
					}).hide();
			});
		}
	});
}

function SearchButton_Click() 
{
	$('.search-language .search-ico').click(function(event) {
		/* Act on the event */
		$('.input-search').toggleClass('input-search-showed', function() {
			alert('aa');
		});
	});
}

function Window_Resize()
{

	$(window).resize(function(event) {
		/* Act on the event */
		
		setBannerHeight();		//update current size
		setSlotBannerWidth();		//update current size
	});
}

function setBannerHeight()
{
	banner_height = banner_width * ratio;
	
	$('.banner').height(banner_height);

	setNewsEventBoardProperties();
}

function setNewsEventBoardProperties()
{
	half_banner_height = banner_height / 2;

	if (banner_height < banner_min_height) {
		half_banner_height = banner_min_height / 2;
	} 

	$('.news-event-highlight').height(half_banner_height);


	var margin_top = half_banner_height * (-1);
	$('.news-event-highlight').css('margin-top', margin_top);
}

function setSlotBannerWidth()
{
	var num_slot	= 4
	var w 			= $('.banner').width() - 140;
	var slot_w 		= w / num_slot; 
	$('.student-parent-teacher-group .slot').css('width', slot_w);
}

function SPTIcon_Click()
{
	$('.student-parent-teacher-group .heading .circle-arrow-ico').click(function(event) {
		/* Act on the event */
		if ( $(this).hasClass('up')) {

			var heading 	= $(this).closest('.heading');
			var h 			= heading.outerHeight();

			heading.css('height', h);	//change auto height to constant height

			var slot 		= heading.closest('.slot');

			slot.animate({
				'margin-top': -200,
			}, 500).css('height', 'auto');		//change height 100% by auto to wraps heading and nav-links height 

			$(this).removeClass('up').addClass('down');		//change icon image
			
		} else {
			var heading 	= $(this).closest('.heading');
			var slot 		= heading.closest('.slot');

			slot.animate({'margin-top': 0}, 500).css('height', '100%');		//restore default value: auto -> 100%

			heading.css('height', '100%');		//restore default value: constant height -> 100%

			$(this).removeClass('down').addClass('up');		//change icon image
		}

		hideNewsEventBoard();
	});
}

function NewsEventIcon_Click()
{
	$('.news-event-caption .circle-arrow-ico').click(function(event) {
		/* Act on the event */
		HideNavLinksSPT();
	});
}

function HideNavLinksSPT()
{
	var list = doc.find('.student-parent-teacher-group .heading .circle-arrow-ico');
	if (list.length > 0) {
		$.each(list, function(key, val) {
			//check item contains class .down
			$(val).css('z-index', 100);
			var flag = false;

			$.each(val.classList, function(key2, val2) {
				if (val2 == 'down') {
					flag = true;
					return true;
				}
			});

			if (flag) {

				var heading 	= $(val).closest('.heading');
				var slot 		= heading.closest('.slot');

				slot.animate({'margin-top': 0}, 500).css('height', '100%');		//restore default value: auto -> 100%

				heading.css('height', '100%');		//restore default value: constant height -> 100%

				$(val).removeClass('down').addClass('up');		//change icon image
			}
		});
	}

	showNewsEventBoard();
}

function showNewsEventBoard()
{
	$('.news-event-highlight').addClass('showed')
						.animate({
							'margin-top': 0
						}, 400);
}

function hideNewsEventBoard()
{
	if ($('.news-event-highlight').hasClass('showed'))
	{
		var margin_top = half_banner_height * (-1);

		$('.news-event-highlight').animate({
			'margin-top': margin_top
			},
			400, function() {
			/* stuff to do after animation is complete */
			$(this).removeClass('showed');
		});
	}
}

function Dropdown_Menu()
{

}
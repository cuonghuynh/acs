<?php get_header(); ?>
<div id="content" class="container">
		<?php if (have_posts()) : ?>

			<?php include (TEMPLATEPATH . '/inc/nav.php' ); ?>

			<?php while (have_posts()) : the_post(); ?>

			<article class="result-item" id="post-<?php the_ID(); ?>">
				<header>
					<h4><a href="<?php the_permalink(); ?>" title=""><?php the_title(); ?></a></h4>
					
				</header>

				<div class="entry">
					<?php the_excerpt(); ?>
				</div>
			</article>	

			<?php endwhile; ?>

			<?php include (TEMPLATEPATH . '/inc/nav.php' ); ?>

		<?php else : ?>

			<h1>Nothing found</h1>

		<?php endif; ?>
</div>
<?php get_footer(); ?>

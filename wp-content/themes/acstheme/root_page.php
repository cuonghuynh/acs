<?php
/*
Template Name: Root Page
*/

	get_header();

	$sub_content 	= get_field('root_sub_content');


	if (have_posts()) : while (have_posts()) : the_post(); 

	$thumb_id = get_post_thumbnail_id();
	$thumb_url = wp_get_attachment_image_src($thumb_id,'full', true);

?>

		<div id="content" class="container single-page twotier">
            <div class="comnimpora-abo">
                <span class="icon-holder"><i class="icon home-ico"></i></span>
                <span class="title">COMNIMPORA ABO</span>
            </div>
            <div class="row main-content">
                <div class="colmask">
                    <div class="col-md-8 col-md-push-4 right-content">
                        <!-- <div class="title">
                            <?php the_title(); ?>
                        </div> -->
                        <div class="content">
                            <?php the_content(); 
                            
                            $current = get_the_id();

                            $args = array(
                                'child_of'  => $current,    // current page id
                                );

                            $child_page_list = get_pages( $args );
                            echo '<div class="child_page_list">';
                            echo '<ul>';
                            foreach ($child_page_list as $key => $item) {
                                if ($item->post_parent == $current) {
                                    echo '<li><h2><a href="' . $item->guid . '" alt="">' . $item->post_title . '</a></h2></li>';    
                                }
                            }
                            echo '</ul>';
                            echo '</div>';
                            ?>
                        </div>
                    </div> <!-- / content right -->

                    <div class="col-md-4 col-md-pull-8 left-content">
                         <?php echo $sub_content; ?>
                    </div> <!-- / content left -->
                </div>
            </div>
        </div> <!-- /content -->

	
<?php 
	endwhile; endif;
	get_footer();
?>

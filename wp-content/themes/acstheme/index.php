<?php get_header(); 

    if ( function_exists( 'ot_get_option' ) ) {

        $welcome_heading            = ot_get_option( 'welcome_heading' );
        $welcome_left_column        = ot_get_option( 'welcome_left_column' );
        $welcome_right_column       = ot_get_option( 'welcome_right_column' );

        $slide_title_one            = ot_get_option( 'slide_title_one' );
        $slide_image_one_top        = ot_get_option( 'slide_image_one_top' );
        $slide_image_one_top        = (empty($slide_image_one_top) ? 'images/banner/slide_image_1_top.jpg' : $slide_image_one_top);
        $slide_image_one_bottom     = ot_get_option( 'slide_image_one_bottom' );
        $slide_image_one_bottom     = (empty($slide_image_one_bottom) ? 'images/banner/slide_image_1_bottom.jpg' : $slide_image_one_bottom);

        $slide_title_two            = ot_get_option( 'slide_title_two' );
        $slide_image_two_top        = ot_get_option( 'slide_image_two_top' );
        $slide_image_two_top        = (empty($slide_image_two_top) ? 'images/banner/slide_image_2_top.jpg' : $slide_image_two_top);
        $slide_image_two_bottom     = ot_get_option( 'slide_image_two_bottom' );
        $slide_image_two_bottom     = (empty($slide_image_two_bottom) ? 'images/banner/slide_image_2_bottom.jpg' : $slide_image_two_bottom);

        $slide_title_three            = ot_get_option( 'slide_title_three' );
        $slide_image_three_top        = ot_get_option( 'slide_image_three_top' );
        $slide_image_three_top        = (empty($slide_image_three_top) ? 'images/banner/slide_image_3_top.jpg' : $slide_image_three_top);
        $slide_image_three_bottom     = ot_get_option( 'slide_image_three_bottom' );
        $slide_image_three_bottom     = (empty($slide_image_three_bottom) ? 'images/banner/slide_image_3_bottom.jpg' : $slide_image_three_bottom);


        $slide_title_four            = ot_get_option( 'slide_title_four' );
        $slide_image_four_top        = ot_get_option( 'slide_image_four_top' );

        $slide_image_four_top        = (empty($slide_image_four_top) ? 'images/banner/slide_image_4_top.jpg' : $slide_image_four_top);

        $slide_image_four_bottom     = ot_get_option( 'slide_image_four_bottom' );
        $slide_image_four_bottom     = (empty($slide_image_four_bottom) ? 'images/banner/slide_image_4_bottom.jpg' : $slide_image_four_bottom);

        $current_student             = ot_get_option( 'current_student' );
        $current_student             = (empty($current_student)) ? 'Add the content' : $current_student;
        $potential_student           = ot_get_option( 'potential_student' );
        $potential_student           = (empty($potential_student)) ? 'Add the content' : $potential_student;
        $parents                     = ot_get_option( 'parents' );
        $parents                     = (empty($parents)) ? 'Add the content' : $parents;
        $teachers                    = ot_get_option( 'teachers' );
        $teachers                    = (empty($teachers)) ? 'Add the content' : $teachers;

    }

?>

        <div class="container banner-wrapper">
            <div id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="false">
                <div class="carousel-inner" role="listbox">


                    <div class="banner item active">
                        <div class="half above" style="background-image: url('<?php echo $slide_image_one_top; ?>');">
                            <div class="top">
                                <div class="title">
                                    <span>
                                    <?php 
                                        if (!empty($slide_title_one)) {
                                            echo $slide_title_one;
                                        } else {
                                            echo 'Add the content';
                                        }
                                    ?>
                                    </span>
                                </div>
                               
                            </div>
                        </div>  <!-- /banner half above -->
                        <div class="half below" style="background-image: url('<?php echo $slide_image_one_bottom; ?>');">
                            <div class="bottom">
                                <div class="row">
                                    <div class="col-md-2 heading">
                                        <?php 
                                            if (!empty($welcome_heading)) {
                                                echo $welcome_heading;
                                            } else {
                                                echo 'Pricipal\'s Welcome';
                                            }
                                        ?>
                                    </div>
                                    <div class="col-md-5">
                                        <?php 
                                            if (!empty($welcome_left_column)) {
                                                echo $welcome_left_column;
                                            } else {
                                                echo 'No content';
                                            }
                                        ?>
                                    </div>
                                    <div class="col-md-5">
                                        <?php 
                                            if (!empty($welcome_right_column)) {
                                                echo $welcome_right_column;
                                            } else {
                                                echo 'No content';
                                            }
                                        ?>
                                    </div>
                                </div>
                            </div>  
                        </div><!-- /banner half below -->
                    </div><!-- /banner -->
                    <div class="banner item">
                        <div class="half above" style="background-image: url('<?php echo $slide_image_two_top; ?>');">
                            <div class="top">
                                <div class="title">
                                    <span>
                                    <?php 
                                        if (!empty($slide_title_two)) {
                                            echo $slide_title_two;
                                        } else {
                                            echo 'Add the content';
                                        }
                                    ?>
                                    </span>
                                </div>
                               
                            </div>
                        </div>  <!-- /banner half above -->
                        <div class="half below" style="background-image: url('<?php echo $slide_image_two_bottom; ?>');">
                            <div class="bottom">
                                <div class="row">
                                    <div class="col-md-2 heading">
                                        <?php 
                                            if (!empty($welcome_heading)) {
                                                echo $welcome_heading;
                                            } else {
                                                echo 'Pricipal\'s Welcome';
                                            }
                                        ?>
                                    </div>
                                    <div class="col-md-5">
                                        <?php 
                                            if (!empty($welcome_left_column)) {
                                                echo $welcome_left_column;
                                            } else {
                                                echo 'No content';
                                            }
                                        ?>
                                    </div>
                                    <div class="col-md-5">
                                        <?php 
                                            if (!empty($welcome_right_column)) {
                                                echo $welcome_right_column;
                                            } else {
                                                echo 'No content';
                                            }
                                        ?>
                                    </div>
                                </div>
                            </div>  
                        </div><!-- /banner half below -->
                    </div><!-- /banner -->
                    <div class="banner item">
                        <div class="half above" style="background-image: url('<?php echo $slide_image_three_top; ?>');">
                            <div class="top">
                                <div class="title">
                                    <span>
                                    <?php 
                                        if (!empty($slide_title_three)) {
                                            echo $slide_title_three;
                                        } else {
                                            echo 'Add the content';
                                        }
                                    ?>
                                    </span>
                                </div>
                                
                            </div>
                        </div>  <!-- /banner half above -->
                        <div class="half below" style="background-image: url('<?php echo $slide_image_three_bottom; ?>');">
                            <div class="bottom">
                                <div class="row">
                                    <div class="col-md-2 heading">
                                        <?php 
                                            if (!empty($welcome_heading)) {
                                                echo $welcome_heading;
                                            } else {
                                                echo 'Pricipal\'s Welcome';
                                            }
                                        ?>
                                    </div>
                                    <div class="col-md-5">
                                        <?php 
                                            if (!empty($welcome_left_column)) {
                                                echo $welcome_left_column;
                                            } else {
                                                echo 'No content';
                                            }
                                        ?>
                                    </div>
                                    <div class="col-md-5">
                                        <?php 
                                            if (!empty($welcome_right_column)) {
                                                echo $welcome_right_column;
                                            } else {
                                                echo 'No content';
                                            }
                                        ?>
                                    </div>
                                </div>
                            </div>  
                        </div><!-- /banner half below -->
                    </div><!-- /banner -->
                    <div class="banner item">
                        <div class="half above" style="background-image: url('<?php echo $slide_image_four_top; ?>');">
                            <div class="top">
                                <div class="title">
                                    <span>
                                    <?php 
                                        if (!empty($slide_title_four)) {
                                            echo $slide_title_four;
                                        } else {
                                            echo 'Add the content';
                                        }
                                    ?>
                                    </span>
                                </div>
                               
                            </div>
                        </div>  <!-- /banner half above -->
                        <div class="half below" style="background-image: url('<?php echo $slide_image_four_bottom; ?>');">
                            <div class="bottom">
                                <div class="row">
                                    <div class="col-md-2 heading">
                                        <?php 
                                            if (!empty($welcome_heading)) {
                                                echo $welcome_heading;
                                            } else {
                                                echo 'Pricipal\'s Welcome';
                                            }
                                        ?>
                                    </div>
                                    <div class="col-md-5">
                                        <?php 
                                            if (!empty($welcome_left_column)) {
                                                echo $welcome_left_column;
                                            } else {
                                                echo 'No content';
                                            }
                                        ?>
                                    </div>
                                    <div class="col-md-5">
                                        <?php 
                                            if (!empty($welcome_right_column)) {
                                                echo $welcome_right_column;
                                            } else {
                                                echo 'No content';
                                            }
                                        ?>
                                    </div>
                                </div>
                            </div>  
                        </div><!-- /banner half below -->
                    </div><!-- /banner -->
       
                </div>
                <!-- Left and right controls -->
                
            </div>

            <?php include (TEMPLATEPATH . '/inc/nav_highlight.php' ); ?>
           </div>  <!-- / banner wrapper -->
        <div id="content" class="container news-event-board">
            <div class="heading">
                <span>
                    <i class="icon arrow-ico-up"></i>
                </span>
                <div class="content">
                    <p><span class="white-bg">NEWS AND </span>EVENTS</p>
                </div>
            </div>
            <div class="row">
                
                <?php include (TEMPLATEPATH . '/inc/latest_events.php' ); ?>

                <?php include (TEMPLATEPATH . '/inc/recent_posts.php' ); ?>

            </div>
        </div> <!-- /content -->
        
<?php get_footer(); ?>
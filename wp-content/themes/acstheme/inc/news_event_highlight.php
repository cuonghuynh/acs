                    <div class="news-event-highlight">
                        <div class="heading">
                            <div class="icon-holder">
                                <i class="icon circle-arrow-ico down"></i>
                            </div>
                            <div class="content">
                                News & Event Highlights
                            </div>
                        </div>
                        <div class="news-event-list">
                            <div class="row">
                                <div class="col-md-6 left">
                                    <ul>

<?php 

    $args = array(
        'showposts'         => 12, 
        'post_type'         => 'post',
        'category_name'     => 'featured',
        'order'             => 'desc',
        );

    $query          = new WP_Query($args);
    
    $featured_posts  = $query->get_posts();

    $break_col = ceil((count($featured_posts) / 2));

    $index = 0;

    while($query->have_posts()) : $query->the_post();   
        
    ?>
                                        <li>
                                            <span class="created-date"><?php the_time( 'j F Y' ); ?></span>
                                            <span class="excerpt"><a href="<?php the_permalink(); ?>" title=""><?php the_title(); ?></a></span>
                                        </li>
<?php   
        $index++;
        if ($index == $break_col) {

                                echo '</ul>';
                            echo '</div>';
                            echo '<div class="col-md-6 right">';
                                echo '<ul>';
        }

    endwhile;
    wp_reset_postdata();
 ?>                    

                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div> <!-- /news-event-board -->                                 
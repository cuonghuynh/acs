					<div class="col-md-4 col-md-pull-8 latest-news left-content">
                        <ul>
<?php 

    /**
     *  Event(9)
     *  Featured(8)
     *  News(10)
     */

	$args = array(
        'showposts'         => 12, 
        'post_type'         => 'post',
        'category__not_in'  => array( 9 ),
        'order'             => 'desc',
        );

	$latest_news = new WP_Query($args);

	while($latest_news->have_posts()) : $latest_news->the_post(); ?>

                        <li>
                            <div class="news-item">
                                <div class="title">
                                	<a href="<?php the_permalink(); ?>" title=""><?php the_title(); ?></a>
                                </div>
                                <div class="created-date"><?php the_time('j F Y'); ?></div>   
                            </div>
                        </li>
<?php
	endwhile;

	wp_reset_postdata();

?>
                            
                        </ul>
                    </div> <!-- / content left -->
<?php 
	$menu_name    				= 'current_student_nav';
	$locations      			= get_nav_menu_locations();
	$current_student_menu		= wp_get_nav_menu_object( $locations[ $menu_name ] );
	$current_student_menu_items	= wp_get_nav_menu_items( $current_student_menu->term_id );	//array

	$menu_name    				= 'potential_student_nav';
	$potential_student_menu			= wp_get_nav_menu_object( $locations[ $menu_name ] );
	$potential_student_menu_items	= wp_get_nav_menu_items( $potential_student_menu->term_id );	//array

	$menu_name    				= 'parents_nav';
	$parents_menu				= wp_get_nav_menu_object( $locations[ $menu_name ] );
	$parents_menu_items			= wp_get_nav_menu_items( $parents_menu->term_id );	//array

	$menu_name    				= 'teacher_nav';
	$teacher_menu				= wp_get_nav_menu_object( $locations[ $menu_name ] );
	$teacher_menu_items			= wp_get_nav_menu_items( $teacher_menu->term_id );	//array

?>

			<div class="banner nav-highlight">
                <div class="half above">
                    <div class="top">
                        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                            <i class="nav-control left icon left-nav-arrow-ico"></i>
                        </a>
                        <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                          <i class="nav-control right icon right-nav-arrow-ico"></i>
                        </a>
                    </div>
                    <?php include (TEMPLATEPATH . '/inc/news_event_highlight.php' ); ?>

                    <div class="bottom">
                        <div class="news-event-caption">
                            <div class="icon-holder">
                                <i class="icon circle-arrow-ico up"></i>
                            </div>
                            <div class="text">
                                <div class="rotate">News & Event Highlights</div>
                            </div>
                        </div>
                        <div class="student-parent-teacher-group">
                            <div class="slot">
                                <div class="heading current-student">
                                    <span>Current Student</span>
                                    <span class="bottom-ico"><i class="icon circle-arrow-ico up"></i></span>
                                </div>
                                <div class="nav-links current-student">
                                	<ul>

                                	<?php 
                             

                                	foreach ($current_student_menu_items as $key => $item) {
                                		if ($item->title == 'empty') {
              								echo '<li></li>';
                                		} else {
                                			echo '<li><a href="' . $item->url . '">' . $item->title . '</a></li>';
                                		}
                                	}

                                	?>

                                    </ul>
                                </div>
                            </div>
                            <div class="slot">
                                <div class="heading potential-student">
                                    <span>Potential Student</span>
                                    <span class="bottom-ico"><i class="icon circle-arrow-ico up"></i></span>
                                </div>
                                <div class="nav-links potential-student">
                                    <ul>

                                    	<?php 
	                                	

	                                	foreach ($potential_student_menu_items as $key => $item) {
	                                		if ($item->title == 'empty') {
	              								echo '<li></li>';
	                                		} else {
	                                			echo '<li><a href="' . $item->url . '">' . $item->title . '</a></li>';
	                                		}
	                                	}

	                                	?>
                
                                    </ul>
                                </div>
                            </div>
                            <div class="slot">
                                <div class="heading parent">
                                    <span>Parents</span>
                                    <span class="bottom-ico"><i class="icon circle-arrow-ico up" ></i></span>
                                </div>
                                <div class="nav-links parent">
                                    <ul>

                                    	<?php 
	                                	

	                                	foreach ($parents_menu_items as $key => $item) {
	                                		if ($item->title == 'empty') {
	              								echo '<li></li>';
	                                		} else {
	                                			echo '<li><a href="' . $item->url . '">' . $item->title . '</a></li>';
	                                		}
	                                	}

	                                	?>

                                    </ul>
                                </div>
                            </div>
                            <div class="slot">
                                <div class="heading teacher">
                                    <span>Teacher</span>
                                    <span class="bottom-ico"><i class="icon circle-arrow-ico up"></i></span>
                                </div>
                                <div class="nav-links teacher">
                                    <ul>

                                    	<?php 
	                                	

	                                	foreach ($teacher_menu_items as $key => $item) {
	                                		if ($item->title == 'empty') {
	              								echo '<li></li>';
	                                		} else {
	                                			echo '<li><a href="' . $item->url . '">' . $item->title . '</a></li>';
	                                		}
	                                	}

	                                	?>

                                       
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>  <!-- /TOP Student - Parent - Teacher Group -->
                </div>  <!-- /banner half above -->
                <div class="half below">
                    <div class="top">
                        <div class="student-parent-teacher-group">
                            <div class="slot">
                                <div class="content current-student">
                                    <?php echo $current_student; ?>
                                </div>
                            </div>
                            <div class="slot">
                                <div class="content potential-student">
                                    <?php echo $potential_student; ?>
                                </div>
                            </div>
                            <div class="slot">
                                <div class="content parent">
                                    <?php echo $parents; ?>
                                </div>
                            </div>
                            <div class="slot">
                                <div class="content teacher">
                                    <?php echo $teachers; ?>
                                </div>
                            </div>
                        </div>
                    </div> <!-- /BOTTOM Student - Parent - Teacher Group -->  
                </div><!-- /banner half below -->
            </div><!-- /banner nav-highlight-->
<?php 
    
    $heading = array(
            'Potential Students'    => 'Information For: ',
            'Parents'               => 'Information For: ',
            'Current Students'      => 'Information For: ',
            'Teachers'              => 'Information For: ',
            'Curriculum'            => 'Information About: ',
            'Curriculum Delivery'   => 'Information About: ',
            'Houses'                => 'Information About: ',
            'Newsletters'           => 'Information About: ',
            'Higher Education'      => 'Information About: ',
            'Community Service'     => 'Information About: ',
            'Heritage'              => 'Who We Are: ',
            'Vision, Mission & Values'              => 'Who We Are: ',
            'School Plan'                           => 'Who We Are: ',
            'Policies'                              => 'Who We Are: ',
            'Achievements'                          => 'Who We Are: ',
            'Corporate Governance'                  => 'Who We Are: ',
            'Academic Team'                         => 'Who We Are: ',
            'Campus Tour'                           => 'Campus: ',
            'Facilities'                            => 'Campus: ',
            'Services'                              => 'Campus: ',
            'Calendar Links'                        => 'Calendar: ',
            'Term Dates'                            => 'Calendar: ',
            'Events'                                => 'Calendar: ',
            'News'                                  => 'Calendar: ',
            'How To Get There'                      => 'Contact Us: ',
            'Email'                                 => 'Contact Us: ',
            'Logins'                                => 'Contact Us: ',
        );
    
?>

    <ul>
    <?php 

        $ancestors = get_post_ancestors( $post );
        $max = count($ancestors);
        $index = 1;
        for ($i=$max-1; $i >= 0 ; $i--) {

            $title = get_the_title( $ancestors[$i] );
            $url = get_permalink( $ancestors[$i] );

            if ($index == 2) {

                echo '<li><span class="parent">' . $heading[$title] . '</span><a href="' . $url . '">' . $title . '</a></li>';

            } else {
                echo '<li><a href="' . $url . '">' . $title . '</a></li>';
            }
            
            $index++;
            
        }
        if ($max == 1) {
            echo '<li class="current"><span class="parent">' . $heading[get_the_title()] . '</span>' . get_the_title() . '</li>';    
        } else {
            echo '<li class="current">' . get_the_title() . '</li>';    
        }
        

    ?>
    </ul>

    <?php 

    if (!is_home()) {
        echo '<ul>';
        if ( is_category() || is_single() ) {
            $cats = get_the_category();
            echo '<li><a href="' . get_category_link( $cats[0]->cat_ID ) . '">' . $cats[0]->name . '</a></li>';
            if ( is_single() ) {
                echo '<li><a href="' . get_permalink() . '">' . get_the_title() . '</a></li>';
            }
        } elseif ( is_page() ) {
            if ($post->post_parent) {
                $anc = get_post_ancestors( $post->ID );
                foreach ($anc as $key => $ancestor) {
                    echo '<li><a href="' . get_permalink($ancestor) . '">' . get_the_title($ancestor) . '</a></li>';
                }
            }
            echo '<li><a href="' . get_permalink() . '">' . get_the_title() . '</a></li>';
        }
        echo '</ul>';
    }


    ?>
				<div class="col-md-3 col-md-pull-9 recent-post">
                    <div class="title">
                        RECENT POSTS
                    </div>
                    <ul>
<?php 
	
    /**
     *  Event(9)
     *  Featured(8)
     *  News(10)
     */

    $args = array(
        'showposts'         => 6, 
        'post_type'         => 'post',
        // 'category__and'     => array( 8, 10 ),
        'category__not_in'  => array ( 9 ),
        'order'           => 'desc',
        );

	$recent_posts = new WP_Query($args);

	while($recent_posts->have_posts()) : $recent_posts->the_post(); ?>

                        <li>
                            <div class="item-label">
                                <a href="<?php the_permalink(); ?>" title=""><?php the_title(); ?></a>
                            </div>
                            <div class="created-date"><?php the_time('j F Y'); ?></div>      
                        </li>
<?php
	endwhile;

	wp_reset_postdata();

?>
 					</ul>
                </div> <!-- / content left -->
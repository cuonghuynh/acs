<?php 
	$locations      					= get_nav_menu_locations();
	
	$menu_name    						= 'information_for_site_map';
	$information_for_site_map			= wp_get_nav_menu_object( $locations[ $menu_name ] );
	$information_for_site_map_name		= $information_for_site_map->name;
	$information_for_site_map_items		= wp_get_nav_menu_items( $information_for_site_map->term_id );	//array

	$menu_name    						= 'information_about_site_map';
	$information_about_site_map			= wp_get_nav_menu_object( $locations[ $menu_name ] );
	$information_about_site_map_name	= $information_about_site_map->name;
	$information_about_site_map_items	= wp_get_nav_menu_items( $information_about_site_map->term_id );	//array

	$menu_name    						= 'who_we_are_site_map';
	$who_we_are_site_map				= wp_get_nav_menu_object( $locations[ $menu_name ] );
	$who_we_are_site_map_name			= $who_we_are_site_map->name;
	$who_we_are_site_map_items			= wp_get_nav_menu_items( $who_we_are_site_map->term_id );	//array

	$menu_name    						= 'campus_site_map';
	$campus_site_map					= wp_get_nav_menu_object( $locations[ $menu_name ] );
	$campus_site_map_name				= $campus_site_map->name;
	$campus_site_map_items				= wp_get_nav_menu_items( $campus_site_map->term_id );	//array

	$menu_name    						= 'calendar_site_map';
	$calendar_site_map					= wp_get_nav_menu_object( $locations[ $menu_name ] );
	$calendar_site_map_name				= $calendar_site_map->name;
	$calendar_site_map_items			= wp_get_nav_menu_items( $calendar_site_map->term_id );	//array

	$menu_name    						= 'contact_us_site_map';
	$contact_us_site_map				= wp_get_nav_menu_object( $locations[ $menu_name ] );
	$contact_us_site_map_name			= $contact_us_site_map->name;
	$contact_us_site_map_items			= wp_get_nav_menu_items( $contact_us_site_map->term_id );	//array


?>

		<footer class="container">
            <div class="site-map desktop">
                <div class="heading title">SITE MAP</div>
                <div class="row content">
                    <div class="col-md-2 wide light information-for">
						<?php 
							$num = count($information_for_site_map_items);
							$current_key = 0;
							
						?>
                        <div class="title"><?php echo $information_for_site_map_name; ?></div>
                        <div class="block">
                        	<ul>
	                        	<?php foreach ($information_for_site_map_items as $key => $item) {
	                        		$current_key = $key;
	                        		if ($item->title == 'break') break;

	                        		$next_item = $information_for_site_map_items[$key+1];

	                        		if ( $next_item->menu_item_parent == '0' ) {
										echo '<li class="last"><a href="' . $item->url .'" title="">' . $item->title .'</a></li>';
	                        		} else {
	                        			echo '<li><a href="' . $item->url .'" title="">' . $item->title .'</a></li>';
	                        		}
	                        		
	                        	} ?>
							</ul>
                        </div>
                        <div class="block">
                            <ul>
                            	<?php for ($i=$current_key+1; $i <= $num; $i++) { 

                            		$next_item = $information_for_site_map_items[$i+1];

                            		if ( $next_item->menu_item_parent == '0' ) {
                            			echo '<li class="last"><a href="' . $information_for_site_map_items[$i]->url .'" title="">' . $information_for_site_map_items[$i]->title .'</a></li>';
                            		} else {
                            			echo '<li><a href="' . $information_for_site_map_items[$i]->url .'" title="">' . $information_for_site_map_items[$i]->title .'</a></li>';	
                            		}
                            		
                            	} ?>

                            </ul>
                        </div>
                    </div> <!-- /information for -->
                    <div class="col-md-2 normal information-about">
                        <div class="title"><?php echo $information_about_site_map_name; ?></div>
						<ul>
						<?php 
							foreach ($information_about_site_map_items as $key => $item) {

								$next_item = $information_about_site_map_items[$key+1];

								if ( $next_item->menu_item_parent == '0' ) {
									echo '<li class="last"><a href="' . $item->url .'" title="">' . $item->title .'</a></li>';
								} else {
									echo '<li><a href="' . $item->url .'" title="">' . $item->title .'</a></li>';
								}
								
							}

						?>
						</ul>

                    </div> <!-- /information about -->
                    <div class="col-md-2 normal light who-we-are">
                        <div class="title"><?php echo $who_we_are_site_map_name; ?></div>
						<ul>
						<?php 
							foreach ($who_we_are_site_map_items as $key => $item) {
								$next_item = $who_we_are_site_map_items[$key+1];

								if ( $next_item->menu_item_parent == '0' ) {
									echo '<li class="last"><a href="' . $item->url .'" title="">' . $item->title .'</a></li>';
								} else {
									echo '<li><a href="' . $item->url .'" title="">' . $item->title .'</a></li>';
								}
								
							}

						?>
						</ul>

                    </div><!-- / who we are -->
                    <div class="col-md-2 narrow campus">
                        <div class="title"><?php echo $campus_site_map_name; ?></div>
						<ul>
						<?php 
							foreach ($campus_site_map_items as $key => $item) {
								$next_item = $campus_site_map_items[$key+1];

								if ( $next_item->menu_item_parent == '0' ) {
									echo '<li class="last"><a href="' . $item->url .'" title="">' . $item->title .'</a></li>';
								} else {
									echo '<li><a href="' . $item->url .'" title="">' . $item->title .'</a></li>';	
								}
								
							}

						?>
						</ul>

                    </div><!-- / campus -->
                    <div class="col-md-2 narrow light calendar">
                        <div class="title"><?php echo $calendar_site_map_name; ?></div>
                        <ul>
                        <?php foreach ($calendar_site_map_items as $key => $item) {
                        		$next_item = $calendar_site_map_items[$key+1];

								if ( $next_item->menu_item_parent == '0' ) {
									echo '<li class="last"><a href="' . $item->url .'" title="">' . $item->title .'</a></li>';
								} else {
									echo '<li><a href="' . $item->url .'" title="">' . $item->title .'</a></li>';
								}
                        	
                        } ?>
						</ul>

                    </div><!-- / calendar -->
                    <div class="col-md-2 normal contact-us">
                        <div class="title"><?php echo $contact_us_site_map_name; ?></div>
                        <ul>
                        	<?php foreach ($contact_us_site_map_items as $key => $item) {
                        		$next_item = $contact_us_site_map_items[$key+1];

								if ( $next_item->menu_item_parent == '0' ) {
									echo '<li class="last"><a href="' . $item->url .'" title="">' . $item->title .'</a></li>';
								} else {
									echo '<li><a href="' . $item->url .'" title="">' . $item->title .'</a></li>';	
								}
                        		
                        	} ?>
                        </ul>

                    </div><!-- / contact us -->
                </div>  
            </div> <!-- desktop version -->

            <div class="site-map mobile">
                <div class="content mobile">
                    <div class="heading title">SITE MAP</div>
                    <div class="panel-group" id="accordion">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
                                    <?php echo $information_for_site_map_name; ?></a>
                                </h4>
                            </div>
                            <div id="collapse1" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div class="block">
                                        <ul>
				                        	<?php foreach ($information_for_site_map_items as $key => $item) {
				                        		$current_key = $key;
				                        		if ($item->title == 'break') break;
				                        		echo '<li><a href="' . $item->url .'" title="">' . $item->title .'</a></li>';
				                        	} ?>
										</ul>
                                    </div>
                                    <div class="block">
                                        <ul>
			                            	<?php for ($i=$current_key+1; $i <= $num; $i++) { 
			                            		echo '<li><a href="' . $information_for_site_map_items[$i]->url .'" title="">' . $information_for_site_map_items[$i]->title .'</a></li>';
			                            	} ?>

			                            </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">
                                    <?php echo $information_about_site_map_name; ?></a>
                                </h4>
                            </div>
                            <div id="collapse2" class="panel-collapse collapse">
                                <div class="panel-body">
                                   	<ul>
									<?php 
										foreach ($information_about_site_map_items as $key => $item) {
											echo '<li><a href="' . $item->url .'" title="">' . $item->title .'</a></li>';
										}

									?>
									</ul>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">
                                    <?php echo $who_we_are_site_map_name; ?></a>
                                </h4>
                            </div>
                            <div id="collapse3" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <ul>
                                    	<?php 
                                    		foreach ($who_we_are_site_map_items as $key => $item) {
                                    			echo '<li><a href="' . $item->url .'" title="">' . $item->title .'</a></li>';
                                    		}
                                    	?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse4">
                                    <?php echo $campus_site_map_name; ?></a>
                                </h4>
                            </div>
                            <div id="collapse4" class="panel-collapse collapse">
                            	<div class="panel-body">
	                            	<ul>
	                            		<?php foreach ($campus_site_map_items as $key => $item) {
	                            			echo '<li><a href="' . $item->url .'" title="">' . $item->title .'</a></li>';
	                            		} ?>
	                            	</ul>
                            	</div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse5">
                                    <?php echo $calendar_site_map_name; ?></a>
                                </h4>
                            </div>
                            <div id="collapse5" class="panel-collapse collapse">
                                <div class="panel-body">
                                   	<ul>
                                   		<?php foreach ($calendar_site_map_items as $key => $item) {
                                   			echo '<li><a href="' . $item->url .'" title="">' . $item->title .'</a></li>';
                                   		} ?>
                                   	</ul>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse6">
                                    <?php echo $contact_us_site_map_name; ?></a>
                                </h4>
                            </div>
                            <div id="collapse6" class="panel-collapse collapse">
                                <div class="panel-body">
                                	<ul>
                                		<?php foreach ($contact_us_site_map_items as $key => $item) {
                                			echo '<li><a href="' . $item->url .'" title="">' . $item->title .'</a></li>';
                                		} ?>

                                	</ul>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div> 
            </div> <!-- mobile version -->
        </footer> <!-- /footer -->
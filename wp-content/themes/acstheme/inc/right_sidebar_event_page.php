					<div class="col-md-4 col-md-pull-8 latest-event left-content">
                        <ul>
<?php 
	
    /**
     *  Event(9)
     *  Featured(8)
     *  News(10)
     */

	$args = array(
        'showposts'         => 14, 
        'post_type'         => 'post',
        'category__not_in'     => array( 10 ),
        'order'           => 'desc',
        );


	$latest_events = new WP_Query($args);

    while($latest_events->have_posts()) : $latest_events->the_post(); 

    	$thumb_id = get_post_thumbnail_id();
        $thumb_url = wp_get_attachment_image_src($thumb_id,'thumbnail', true);
?>
                            <li>
                                <div class="event-item">
                                    <div class="image">
                                        <a href="<?php the_permalink(); ?>"><img src="<?php echo $thumb_url['0']; ?>" alt=""></a>
                                    </div>
                                    <div class="title"><a href="<?php the_permalink(); ?>" title=""><?php the_title(); ?></a></div>
                                    <div class="created-date"><?php the_time( 'j M' ); ?></div>
                                </div>
                            </li>
<?php
    endwhile;

    wp_reset_postdata();
?>
                        </ul>
                    </div> <!-- / content left -->
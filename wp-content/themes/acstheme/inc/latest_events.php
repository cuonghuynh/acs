                <div class="col-md-9 col-md-push-3 latest-event">
                    <ul class="row">

<?php 
    
    /**
     *  Event(9)
     *  Featured(8)
     *  News(10)
     */

    $args = array(
        'showposts'         => 6, 
        'post_type'         => 'post',
        'category__not_in'  => array( 10 ),
        'order'           => 'desc',
        );

    $latest_events = new WP_Query($args);

    $latest_events;

    $counter = 0;

    while($latest_events->have_posts()) : $latest_events->the_post(); 
        $counter++;

        $thumb_id = get_post_thumbnail_id();
        $thumb_url = wp_get_attachment_image_src($thumb_id,'thumbnail', true);
        

?>

                        <li class="col-md-4">
                            <div class="image">
                                <a href="<?php the_permalink(); ?>" title="">
                                    <img src="<?php echo $thumb_url[0]; ?>" alt="">
                                </a>
                                <div class="created-date">
                                    <span><?php the_time( 'j M' ); ?></span>
                                </div>
                            </div>
                            <div class="item-label">
                                <?php the_title(); ?>
                            </div>
                            <div class="description">
                                <a href="<?php the_permalink(); ?>" title=""><?php the_excerpt(); ?></a>
                            </div>
                                
                        </li>

        <?php 
            if ($counter >= 3) {
                $counter = 0;
                echo '</ul>';
                echo '<ul class="row">';
            }
        ?>
<?php
    endwhile;

    wp_reset_postdata();
?>
                    </ul>
                </div> <!-- / content right -->
                      
                        <!-- <li class="col-md-4">
                            <div class="image">
                                <a href="" title="">
                                    <img src="images/post/event_thumb_2.jpg" alt="">
                                </a>
                                <div class="created-date">
                                    <span>21 NOV</span>
                                </div>
                            </div>
                            <div class="item-label">
                                Event latest 01
                            </div>
                            <div class="description">
                                <a href="" title="">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </a>
                            </div>
                            <div class="more">
                                <a href="" title=""><i class="icon more-arrow-ico"></i></a>
                            </div>
                        </li>
                        <li class="col-md-4">
                            <div class="image">
                                <a href="" title="">
                                    <img src="images/post/event_thumb_3.jpg" alt="">
                                </a>
                                <div class="created-date">
                                    <span>21 NOV</span>
                                </div>
                            </div>
                            <div class="item-label">
                                Event latest 01
                            </div>
                            <div class="description">
                                <a href="" title="">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </a>
                            </div>
                            <div class="more">
                                <a href="" title=""><i class="icon more-arrow-ico"></i></a>
                            </div>
                        </li>
                    </ul>
                    <ul class="row">
                        <li class="col-md-4">
                            <div class="image">
                                <a href="" title="">
                                    <img src="images/post/event_thumb_4.jpg" alt="">
                                </a>
                                <div class="created-date">
                                    <span>21 NOV</span>
                                </div>
                            </div>
                            <div class="item-label">
                                Event latest 01
                            </div>
                            <div class="description">
                                <a href="" title="">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </a>
                            </div>
                            <div class="more">
                                <a href="" title=""><i class="icon more-arrow-ico"></i></a>
                            </div>
                        </li>
                        <li class="col-md-4">
                            <div class="image">
                                <a href="" title="">
                                    <img src="images/post/event_thumb_5.jpg" alt="">
                                </a>
                                <div class="created-date">
                                    <span>21 NOV</span>
                                </div>
                            </div>
                            <div class="item-label">
                                Event latest 01
                            </div>
                            <div class="description">
                                <a href="" title="">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </a>
                            </div>
                            <div class="more">
                                <a href="" title=""><i class="icon more-arrow-ico"></i></a>
                            </div>
                        </li>
                        <li class="col-md-4">
                            <div class="image">
                                <a href="" title="">
                                    <img src="images/post/event_thumb_1.jpg" alt="">
                                </a>
                                <div class="created-date">
                                    <span>21 NOV</span>
                                </div>
                            </div>
                            <div class="item-label">
                                Event latest 01
                            </div>
                            <div class="description">
                                <a href="" title="">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </a>
                            </div>
                            <div class="more">
                                <a href="" title=""><i class="icon more-arrow-ico"></i></a>
                            </div>
                        </li>
                     -->
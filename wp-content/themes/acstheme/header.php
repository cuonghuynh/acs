<!DOCTYPE html>

<html <?php language_attributes(); ?>>

<head>
     <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="<?php bloginfo('charset'); ?>" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">
    

    <?php if (is_search()) { ?>
        <meta name="robots" content="noindex, nofollow" />
    <?php } ?>

    <title>
        <?php
        if (function_exists('is_tag') && is_tag()) {
            single_tag_title("Tag Archive for &quot;"); echo '&quot; - '; }
        elseif (is_archive()) {
            wp_title(''); echo ' Archive - '; }
        elseif (is_search()) {
            echo 'Search for &quot;'.wp_specialchars($s).'&quot; - '; }
        elseif (!(is_404()) && (is_single()) || (is_page())) {
            wp_title(''); echo ' - '; }
        elseif (is_404()) {
            echo 'Not Found - '; }
        if (is_home()) {
            bloginfo('name'); echo ' - '; bloginfo('description'); }
        else {
            bloginfo('name'); }
        if ($paged>1) {
            echo ' - page '. $paged; }
        ?>
    </title>
    
    <link href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/css/menu.css" rel="stylesheet">
    <link href="data:text/css;charset=utf-8," data-href="<?php echo get_template_directory_uri(); ?>/css/bootstrap-theme.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed&subset=cyrillic,cyrillic-ext,latin,latin-ext,vietnamese' rel='stylesheet' type='text/css'>
    <link href="<?php bloginfo('stylesheet_url'); ?>" rel="stylesheet">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

    <script type="text/javascript" src="<?php echo get_bloginfo('template_url'); ?>/js/modernizr-1.5.min.js"></script>

    <?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>

    <?php wp_head(); ?>

    <!-- Theme generate -->
    <style type="text/css" media="screen">
        nav ul li {
            width: 20%;     /*Depend on menu item count*/
        }
    </style>
    
</head>

<body <?php body_class(); ?>>
    <div class="loader"></div>
    <div id="wrapper">
        <header class="container">
            <div class="bar sm-text top">
                <div class="school-name">acs international school</div>
                <div class="search-language">
                    <div class="search-holder">
                        <form action="<?php bloginfo('siteurl'); ?>" id="searchform" method="get" class="search-form-top">
                            <div>
                              <input type="search" id="s" name="s" value="" class="input-search" />
                              
                          </div>
                        </form>
                        <div class="search-wrapper">
                            <ul>
                                <li><i class="icon search-ico"></i></li>
                                <li>
                                    <i class="icon language-switcher-ico"></i>
                                    <span class="current-language"><?php echo ICL_LANGUAGE_NAME_EN; ?></span>
                                     <ul class="language-list">
                                        <?php
                                            $languages = icl_get_languages('skip_missing=0');
                                            foreach ($languages as $key => $language) {
                                                echo '<li><a href=" ' . $language['url'] . ' " title="">' . $language['translated_name'] . '</a></li>';
                                            }
                                        ?>
                                    </ul>
                                </li>
                            </ul>  
                        </div>
                    </div>
                </div>
            </div> <!-- / Search - Language -->
            <div class="logo-holder">
                <?php 

                if ( function_exists( 'ot_get_option' ) ) {

                    $logo           = ot_get_option( 'logo_image' );
                    $facebook       = ot_get_option( 'facebook' );
                    $pinterest      = ot_get_option( 'pinterest' );
                    $twitter        = ot_get_option( 'twitter' );
                    $linkedin       = ot_get_option( 'linkedin' );
                }


                ?>
                <a href="<?php echo home_url(); ?>" class="logo">
                    <?php 
                        if (!empty($logo)) { ?>
                            <img src="<?php echo $logo; ?>" alt="<?php bloginfo( 'name' ); ?>">
                       <?php } else { ?>
                            <img src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt="<?php bloginfo( 'name' ); ?>">
                       <?php }
                    ?>                    
                </a>
                <div class="social-group">
                    <ul>
                        <?php
                            if (!empty($facebook)) {
                                echo '<li><a href="' . $facebook . '" title=""><i class="icon facebook-ico"></i></a></li>';
                            }

                            if (!empty($pinterest)) {
                                echo '<li><a href="' . $pinterest . '" title=""><i class="icon pinterest-ico"></i></a></li>';
                            }

                            if (!empty($twitter)) {
                                echo '<li><a href="' . $twitter . '" title=""><i class="icon twitter-ico"></i></a></li>';
                            }

                            if (!empty($linkedin)) {
                                echo '<li><a href="' . $linkedin . '" title=""><i class="icon linkedin-ico"></i></a></li>';
                            }
                        ?>
                    </ul>
                </div>
            </div>   <!-- / Logo -->
            <!-- <div class="bar bottom"> -->
                <?php 
                    $menu = array(                                                  
                        'menu'                  => 21,
                        'container'             => 'div',
                        'container_id'          => 'cssmenu',
                        'menu_class'            => '',
                            ); 
                    
                    wp_nav_menu($menu);
                ?>
                
            <!-- </div> -->
        </header><!-- /header -->
<?php
/*
Template Name: Intro Page
*/

	get_header();

	$_title 			= get_field('banner_title');
    
	$sub_content 	= get_field('sub_content');


	if (have_posts()) : while (have_posts()) : the_post(); 

	$thumb_id = get_post_thumbnail_id();
	$thumb_url = wp_get_attachment_image_src($thumb_id,'full', true);

?>

		<div id="content" class="container single-page twotier">
            <div class="intro-panel" style="background-image: url('<?php echo $thumb_url[0]; ?>');">
                <div class="breadcrumb">
                    <?php include( TEMPLATEPATH . '/inc/breadcrumb.php'); ?>
                </div>
                <div class="intro-text">
                    <span><?php echo $_title; ?></span>
                    <i class="left-bracket-ico"></i>
                    <i class="right-bracket-ico"></i>
                </div>
            </div>
            <div class="comnimpora-abo">
                <span class="icon-holder"><i class="icon home-ico"></i></span>
                <span class="title">COMNIMPORA ABO</span>
            </div>
            <div class="row main-content">
                <div class="colmask">
                    <div class="col-md-8 col-md-push-4 right-content">
                        <!-- <div class="title">
                            <?php the_title(); ?>
                        </div> -->
                        <div class="content">
                            <?php the_content(); ?>
                        </div>
                    </div> <!-- / content right -->

                    <div class="col-md-4 col-md-pull-8 left-content">
                         <?php echo $sub_content; ?>
                    </div> <!-- / content left -->
                </div>
            </div>
        </div> <!-- /content -->

	
<?php 
	endwhile; endif;
	get_footer();
?>

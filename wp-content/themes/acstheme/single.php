<?php get_header(); ?>

    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

        <div id="content" class="container news-page">
            <div class="breadcrumb">
                <?php 
                    $cat = get_the_category();
                    $cat_name = $cat[0]->name;
                    $url = get_category_link( $cat[0]->term_id );
                 ?>
                <ul>
                    <li>CALENDAR</li>
                    <li class="current cat"><a href="<?php echo $url; ?>" title=""><?php echo $cat_name; ?></a></li>
                </ul>
            </div>
            <div class="row">
                <div class="colmask">
                    <div class="col-md-8 col-md-push-4 news-content right-content">
                        <div class="title">
                            <?php the_title(); ?>
                        </div>
                        <div class="content">
                            <?php the_content(); ?>
                        </div>
                    </div> <!-- / content right -->
                    
                    <?php 
                        $cats = get_the_category(get_the_id());

                        $current_post_cats = array();

                        foreach ($cats as $key => $cat) {
           
                            $current_post_cats[$key] = $cat->slug;
                        }

                        if (in_array('news', $current_post_cats)) {

                            include (TEMPLATEPATH . '/inc/right_sidebar_news_page.php' );

                        } else {

                            include (TEMPLATEPATH . '/inc/right_sidebar_event_page.php' ); 
                            
                        }
                    ?>
                </div>
                
            </div>
        </div> <!-- /content -->


    <?php endwhile; endif; ?>


<?php get_footer(); ?>